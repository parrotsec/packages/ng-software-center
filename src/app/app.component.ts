import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  categories = [
    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },
    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },
    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },
    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },
    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },    {
      name: 'Category Name',
      featured: [
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        },
        {
          icon: "app-icon.png",
          name: "App Name"
        }
      ]
    },
  ]
}
